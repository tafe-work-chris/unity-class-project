﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameoverdisplay : MonoBehaviour
{
    public Transform cam;
    public Transform player;
    public GameObject Player;

    private void Start()
    {
        Player.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    private void Update()
    {
        Time.timeScale = 0;
        cam.rotation = new Quaternion(0, 0, 0, 0);
        player.rotation = new Quaternion(0, 0, 0, 0);
    }
}
