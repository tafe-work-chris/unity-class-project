﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Timer : MonoBehaviour
{
    //creates a time float so other gameobjects can reference
    //creates a bool to start timer
    public float timer;
    public bool startTimer;
    public void Update()
    {
        //sets bool to true as soon as any input detected
        if (Input.anyKeyDown)
        {
            startTimer = true;
        }
        //if bool true timer is based on deltatime
        if (startTimer)
        {
            timer += Time.deltaTime;
        }
        
    }
}
