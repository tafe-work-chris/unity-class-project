﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class timedisplay : MonoBehaviour
{
    //grabs timer from player
    public Timer player;
    
    //displays time using the text component of gameobject
    public void Update()
    {
        var time = player.timer;
        this.gameObject.GetComponent<Text>().text = time.ToString();
        
    }

}
