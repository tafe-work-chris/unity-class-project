﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Playermovment : MonoBehaviour
{
    // floats that can be adjusted to increase or decrease player movements
    //need pausegame to be activated on input
    //need gameover activated if player health reaches 0
    public float speed = 6;
    public float sensitivity = 1;
    public float jumpSpeed = 0.9f;
    public Transform cam;
    public bool playerSafe;
    public GameObject pausemenu;
    public float playerHealth = 3;
    public GameObject gameover;
    


    // Update is called once per frame
    void Update()
    {
        Movement();
        PauseGame();
       
        //restricts player rotation to only the y axis
        var camrot = transform.eulerAngles;
        camrot.x = 0;
        camrot.y = cam.rotation.eulerAngles.y;
        camrot.z = 0;
        transform.eulerAngles = camrot;

        transform.rotation = Quaternion.Euler(camrot);

        if (playerHealth == 0)
        {
            gameover.SetActive(true);
        }

    }
    // movement function checks if statement (this being a keycode) and then translates the vector3 direction relative to the public transform
    void Movement()
    {

        // applies sprint movement
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = 12;
        }
        else
        {
            speed = 6;
        }

        //have multiplied by speed, so that speed changes come into effect and time.deltatime to keep it separate from the framerate.
        if (Input.GetKey(KeyCode.W))
        {

            transform.Translate(transform.forward * speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.S))
        {

            transform.Translate(transform.forward * -speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.A))
        {

            transform.Translate(transform.right * -speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.D))
        {

            transform.Translate(transform.right * speed * Time.deltaTime, null);

        }
    }

    //opens the pause menu on input of player
    //sets the timescale to 0 and restricts the rotation of player and rotation of cam
    //sets the cursor lockmode to none and visibility to true
    public void PauseGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pausemenu.SetActive(true);
        }

        if (pausemenu.activeInHierarchy)
        {
            Time.timeScale = 0;
            transform.rotation = new Quaternion(0, 0, 0, 0);
            cam.transform.rotation = new Quaternion(0, 0, 0, 0);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        else
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

    }

}


