﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Rigidbody bullet;
    public Transform Camera;
    // Update is called once per frame
    void Update()
    {
        raycast();
    }

    void raycast()
    {

        var ray = new Ray(transform.localPosition, transform.forward);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 100);
        var rayLength =  hit.transform.localPosition - transform.localPosition;
        
        if (Input.GetKey(KeyCode.Mouse1))
        {
            Debug.DrawRay(transform.localPosition, rayLength, Color.red);

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {

                var bulletclone = Instantiate(bullet, new Vector3(Camera.position.x, 3, Camera.position.z), Quaternion.identity);
                bulletclone.velocity = Camera.forward * 200;
            }
        }

       




    }

}
