﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTest : MonoBehaviour
{
    public Rigidbody player;
    public float speed = 2;
    public Vector3 forward;
    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(h, 0, v);
        move = move.normalized * speed * Time.deltaTime;

        player.MovePosition(transform.position + move);
    }

  

}
