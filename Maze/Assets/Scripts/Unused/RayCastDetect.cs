﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastDetect : MonoBehaviour
{
    private Ray playerdetection;
    RaycastHit hit;
    public Collider player;

    // Update is called once per frame
    void Update()
    {
        Raycast();
        HitDetect();
        
    }


    public void Raycast()
    {
        var raylength = Vector3.Distance(GameObject.Find("WallR").transform.position, GameObject.Find("WallL").transform.position) - 0.5f;
        playerdetection = new Ray(transform.position, transform.forward * raylength);

        Physics.Raycast(playerdetection, out hit);

        Debug.DrawRay(transform.position, transform.forward * raylength, Color.blue);
        
    }

    public bool HitDetect()
    {

        if (hit.collider.CompareTag("Player"))
        {
            print("player has entered corridor");
            print("true");
            return true;
        }
        else
        {
            print("false");
            return false;
        }

    }

    
}
