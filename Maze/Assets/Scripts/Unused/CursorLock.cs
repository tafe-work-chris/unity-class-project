﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorLock : MonoBehaviour
{
    public GameObject gameover;
    public GameObject pausemenu;
    public GameObject gamecomplete;
    // creates a toggle for cursor lockmode. turning on gives more realistic game view for tests

    private void Update()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("maze") && !gameover.activeInHierarchy || !pausemenu.activeInHierarchy || !gamecomplete.activeInHierarchy)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible.Equals(false);
        }

        if (gameover.activeInHierarchy || pausemenu.activeInHierarchy || gamecomplete.activeInHierarchy)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible.Equals(true);
        }
    }


}
