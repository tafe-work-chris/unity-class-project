﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    // sensitivity is adjustable float to increase or decrease the amount of degrees from axis movement
    public float sensitivity;
    public Transform Player;
    // Update is called once per frame
    void Update()
    {
        Rotation();
        Movement();
    }

    //movement function handles axis input and converts the floats to euler angles which can then be used as quaternion values to rotate the vector3
    public void Rotation()
    {
        //pitch is rotation around x axis & yaw is rotation around y axis. get axis makes use of auto smoothing (keeps the code concise). also by  multiplying the mouse y axis by -1 we eliminate inversion
        var yaw = Input.GetAxis("Mouse X");
        var pitch = (Input.GetAxis("Mouse Y")) * -1;

        //without this code, transforming the vector3 by eulersAngles would also transform the z axis. as seen setting the z axis to 0 stops this. also seen the x and y axis are set to += yaw and pitch variables multiplied by sensitivity. this allows sensitivity float to come into effect and the rotations are added to current rather than rotating relative to origin and being forced to 0 every frame
        var direction = transform.eulerAngles;
        direction.x += pitch * sensitivity;
        direction.y += yaw * sensitivity;
        direction.z = 0;
        transform.eulerAngles = direction;
        
        //the rotation is now applied by our euler angles stored in direction variable
        transform.rotation = Quaternion.Euler(direction);


    }

    public void Movement()
    {
        transform.position = new Vector3(Player.position.x, 7.17f, Player.position.z);
    }
}
