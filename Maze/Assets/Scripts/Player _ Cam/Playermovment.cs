﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovment : MonoBehaviour
{
    // floats that can be adjusted to increase or decrease player movements
    public float speed = 4;
    public float sensitivity = 1;
    public float jumpSpeed = 0.9f;
    public Transform cam;
    // creates a public transform element to move the player relativeTo
    

    // Update is called once per frame
    void Update()
    {
        Movement();

        var customrot = transform.eulerAngles;
        customrot.x = 0;
        customrot.y = cam.eulerAngles.y;
        customrot.z = 0;
        transform.rotation = Quaternion.Euler(customrot);

    }
    // movement function checks if statement (this being a keycode) and then translates the vector3 direction relative to the public transform
    void Movement()
    {

        // applies sprint movement
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = 12;
        }
        else
        {
            speed = 4;
        }

        //have multiplied by speed, so that speed changes come into effect and time.deltatime to keep it separate from the framerate.
        if (Input.GetKey(KeyCode.W))
        {

            transform.Translate(transform.forward * speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.S))
        {

            transform.Translate(transform.forward * -speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.A))
        {

            transform.Translate(transform.right * -speed * Time.deltaTime, null);

        }
        if (Input.GetKey(KeyCode.D))
        {

            transform.Translate(transform.right * speed * Time.deltaTime, null);

        }
    }

    

}


