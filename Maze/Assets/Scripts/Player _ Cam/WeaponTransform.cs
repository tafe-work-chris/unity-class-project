﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTransform : MonoBehaviour
{
    public Transform Camera;
    public Transform Player;
   
    void Update()
    {
        transform.rotation = Camera.transform.rotation;
        transform.position = new Vector3(Player.transform.position.x, transform.position.y, Player.transform.position.z);
    }
}
