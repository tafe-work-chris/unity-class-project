﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TomeInfo : MonoBehaviour
{
    //if tome info is displayed pausetime and restrict rotation of player and cam
    //if left mouse buton clicked then return to game
    public Transform player;
    public Transform cam;

    public void Update()
    {
        if (this.gameObject.activeInHierarchy)
        {
            Time.timeScale = 0;
            player.transform.rotation = new Quaternion(0, 0, 0, 0);
            cam.transform.rotation = new Quaternion(0, 0, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Time.timeScale = 1;
            this.gameObject.SetActive(false);
        }
    }

}
