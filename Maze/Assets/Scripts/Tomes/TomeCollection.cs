﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TomeCollection : MonoBehaviour
{
    //need to specify which tomeinfo to display
    public GameObject tomeInfo;

    //if player enters tome they will collect it
    //specified tomeinfo will be displayed
    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        tomeInfo.SetActive(true);
    }      
    
}
