﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalActivation : MonoBehaviour
{
    //need to grab gameobject tome so linking is possible and portal end to specify where the player will end up.
    public GameObject tome;
    public Transform player;
    public GameObject portalend;
    // Start is called before the first frame update

    //on entering portal need to check if specified tome has been collected.
    //if true then teleport player to position within maze.
    private void OnTriggerEnter(Collider other)
    {
        if (!tome.activeInHierarchy)
        {
            player.transform.position = new Vector3(portalend.transform.position.x, 1.2f, portalend.transform.position.z);
        }
    }
}
