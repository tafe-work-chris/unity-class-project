﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class HealthDisplay : MonoBehaviour
{
    public GameObject health1;
    public GameObject health2;
    public GameObject health3;

    private void Start()
    {
        //on starting game set health to active
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("maze"))
        {
            health1.SetActive(true);
            health2.SetActive(true);
            health3.SetActive(true);
        }
    }
}
