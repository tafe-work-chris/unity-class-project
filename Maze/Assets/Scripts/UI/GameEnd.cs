﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using System.Globalization;


public class GameEnd : MonoBehaviour
{   
    //on reaching end game triggerzone display gamecomplete screen
    public GameObject gamecomplete;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            gamecomplete.SetActive(true);
        }
        
    }
}
