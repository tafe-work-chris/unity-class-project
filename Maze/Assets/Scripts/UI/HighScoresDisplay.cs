﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class HighScoresDisplay : MonoBehaviour
{
    //grabbing gameobjects that hold the text components wea re writing to.
    public GameObject highAlias;
    public GameObject highestscore;
    public GameObject midAlias;
    public GameObject middlescore;
    public GameObject lowAlias;
    public GameObject lowestscore;


    private void Start()
    {
        //reads all text from score file
        // creates string separated by spaces and new lines
        var scoreString = File.ReadAllText(Application.dataPath + "/Scores").ToString();
        string[] scores = scoreString.Split(new char[] { ' ', '\n' });


        var highScore0 = 0f;
        var highScore1 = 0f;
        var highScore2 = 0f;

        var scoreLength = (scores.Length) - 1;
        print(scoreLength);

        // if scorelength is desired number parse the array i as a float.
        //using try parse first elminates exception as it returns false instead of input string not correct format
        if (scoreLength >= 3)
        {
            bool check0 = float.TryParse(scores[3], out highScore0);
            if (check0)
            {
                highScore0 = float.Parse(scores[3]);
            }
        }

        if (scoreLength >= 5)
        {
            bool check1 = float.TryParse(scores[5], out highScore1);
            if (check1)
            {
                highScore1 = float.Parse(scores[5]);
            }
        }

        if (scoreLength >= 7)
        {
            bool check2 = float.TryParse(scores[7], out highScore2);
            if (check2)
            {
                highScore2 = float.Parse(scores[7]);
            }
        }

        //creates new float array of parsed values
        float[] HighScores = new float[] { highScore0, highScore1, highScore2 };
        print(HighScores[0]);
        print(HighScores[1]);
        print(HighScores[2]);
        //second float for storing mid and low score once high score been identified
        float[] HighScores2 = new float[] { 0, 0 };

        var highestScoreIndex = 0;
        var middleScoreIndex = midAlias.GetComponent<Text>().text;
        var lowestScoreIndex = lowAlias.GetComponent<Text>().text;

        var highestScore = Mathf.Min(HighScores);
        var middleScore = 0f;
        var lowestScore = 0f;

        //if scorelength is desired length identify the highest mid and low score
        //set the index accordingly to retrieve the correct alias name from scores[]
        if (scoreLength >= 3)
        {
            highestScore = HighScores[0];
            highestScoreIndex = 2;
        }

        if (scoreLength >= 5)
        {
            highestScore = Mathf.Min(highScore0, highScore1);
            if (highestScore == HighScores[0])
            {
                highestScoreIndex = 2;
                middleScoreIndex = 4.ToString();
                middleScore = HighScores[1];
            }

            else
            {
                middleScoreIndex = 2.ToString();
                middleScore = HighScores[0];
                highestScoreIndex = 4;
            }

        }

        if (scoreLength >= 7)
        {
            if (highestScore == HighScores[0])
            {
                HighScores2[0] = highScore1;
                HighScores2[1] = highScore2;
                highestScoreIndex = 2;

                middleScore = Mathf.Min(HighScores[1], HighScores[2]);
                print(middleScore);
                if (middleScore == HighScores[1])
                {
                    lowestScore = HighScores[2];
                    middleScoreIndex = 4.ToString();
                    lowestScoreIndex = 6.ToString();
                }

                else
                {
                    lowestScore = HighScores[1];
                    lowestScoreIndex = 4.ToString();
                    middleScoreIndex = 6.ToString();
                }

              
            }


            if (highestScore == HighScores[1])
            {
                HighScores2[0] = highScore0;
                HighScores2[1] = highScore2;
                highestScoreIndex = 4;

                middleScore = Mathf.Min(HighScores[0], HighScores[2]);
                if (middleScore == HighScores[0])
                {
                    lowestScore = HighScores[2];
                    middleScoreIndex = 2.ToString();
                    lowestScoreIndex = 6.ToString();
                }

                else
                {
                    lowestScore = HighScores[0];
                    lowestScoreIndex = 2.ToString();
                    middleScoreIndex = 6.ToString();
                }
               
            }


            if (highestScore == HighScores[2])
            {
                HighScores2[0] = highScore0;
                HighScores2[1] = highScore1;
                highestScoreIndex = 6;

                middleScore = Mathf.Min(HighScores[0], HighScores[1]);
                if (middleScore == HighScores[1])
                {
                    lowestScore = HighScores[0];
                    middleScoreIndex = 2.ToString();
                    lowestScoreIndex = 4.ToString();
                }

                else
                {
                    lowestScore = HighScores[1];
                    lowestScoreIndex = 2.ToString();
                    middleScoreIndex = 4.ToString();
                }
            
            }
        
        }

        print(highestScore);
        print(middleScore);
        print(lowestScore);
        print(middleScoreIndex);

        //writing the alias and score to the gameobjects text components
        highAlias.GetComponent<Text>().text = scores[highestScoreIndex].ToString();
        highestscore.GetComponent<Text>().text = (Mathf.Round(highestScore * 10) / 10).ToString();
        midAlias.GetComponent<Text>().text = scores[int.Parse(middleScoreIndex)].ToString();
        middlescore.GetComponent<Text>().text = (Mathf.Round(middleScore * 10) / 10).ToString();
        lowAlias.GetComponent<Text>().text = scores[int.Parse(lowestScoreIndex)].ToString();
        lowestscore.GetComponent<Text>().text = (Mathf.Round(lowestScore * 10) / 10).ToString();
    }

}
