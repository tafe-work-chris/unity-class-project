﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreDisplay : MonoBehaviour
{
    //grabs score from player
    public Timer player;
    public Transform human;
    public Transform cam;
    private void Update()
    {
        //displays score on screen and restricts rotation of player and camera
        this.gameObject.GetComponent<Text>().text = player.timer.ToString();
        if (this.gameObject.activeInHierarchy)
        {
            Time.timeScale = 0;
            human.transform.rotation = new Quaternion(0, 0, 0, 0);
            cam.transform.rotation = new Quaternion(0, 0, 0, 0);
        }

    }
}
