﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    //function to be called on clicking quit to menu button
    public void QuitToMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }
}
