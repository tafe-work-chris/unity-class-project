﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CompletionScript : MonoBehaviour
{
    //on completion of game stop timer stop display of timer and deactivate player
    //set cursor lockmode to none and visible to true
    public Timer timer;
    public float score;
    public GameObject timerdisplay;
    public GameObject player;

    private void Start()
    {
        timerdisplay.SetActive(false);
        timer.startTimer = false;
        score = timer.timer;
        player.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}

