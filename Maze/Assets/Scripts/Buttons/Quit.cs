﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour
{
    //public void to be referenced on quit button from main menu, pause menu, gameover and gamecomplete menus.
    public void QuitGame()
    {
        Application.Quit();
    }

    public void OnApplicationQuit()
    {
        print("success");
    }
}
