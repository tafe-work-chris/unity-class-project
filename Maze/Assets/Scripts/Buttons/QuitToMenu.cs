﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitToMenu : MonoBehaviour
{
    //public void to be referenced on quit to menu button from pause menu, gameover and gamecomplete menus.
    public void Quit()
    {
        SceneManager.LoadScene("StartMenu");
    }
}
