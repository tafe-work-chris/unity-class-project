﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class SaveScore : MonoBehaviour
{
    //grabscore is used to take the player score from the completion script. the alias gameobject is the text that the player enters in the inputfield text component.
    public CompletionScript grabscore;
    public GameObject alias;

    //highscores void is run on click of savescore button.
    public void HighScores()
    {
        //assigns string variable to text entered in inputfield.
        name = alias.GetComponent<Text>().text;

        //specifies location of scores file. if the file does not already exist creates one and writes first score. if the file exists, text is added. 
        //format is name(inputfield text), space, playerscore(from completion script), newline.
        string path = Application.dataPath + "/Scores";
        if (!File.Exists(path))
        {
            File.WriteAllText(path, name + " " + grabscore.score + "\n");
        }

        if (File.Exists(path))
        {
             File.AppendAllText(path, name + " " + grabscore.score + "\n");          
        }

        //creates string from all text saved in scores file. then converts to a string array separated on the basis of a space and a new line which is stored in scores variable.
        var scoreString = File.ReadAllText(Application.dataPath + "/Scores").ToString();
        string[] scores = scoreString.Split(new char[] { ' ', '\n' });

        print(scores.ToString());

        // variable storing length of array
        var scoreLength = (scores.Length) -1;
        print(scoreLength);
        print(scoreString);
        print(scores.Length);

        //variables storing the default values of the higscores and the check bools.
        var highScore0 = 0f;
        var highScore1 = 0f;
        var highScore2 = 0f;
        var baseScore = 0f;
        bool check0 = false;
        bool check1 = false;
        bool check2 = false;
      
        //if able to parse the scores[i] value as a float returns true to the check variable and outputs to the score variable. 
        //without using tryparse before parse, exception is thrown.
        //using if statements will only tryparse if certain amount of scores are saved.
        bool checkbase;
        checkbase = float.TryParse(scores[1], out baseScore);
        if (checkbase)
        {
            baseScore = float.Parse(scores[1]);
        }

        if (scoreLength >= 4)
        {
            check0 = float.TryParse(scores[3], out highScore0);
            if (check0)
            {
                highScore0 = float.Parse(scores[3]);
            }
        }


        if (scoreLength >= 6)
        {
            check1 = float.TryParse(scores[5], out highScore1);
            if (check1)
            {
                highScore1 = float.Parse(scores[5]);
            }

        }

        if (scoreLength >= 8)
        {
            check2 = float.TryParse(scores[7], out highScore2);
            if (check2)
            {
                highScore2 = float.Parse(scores[7]);
            }
        }
      
        //if all checks have succeeded and an extra score added we need to replace the lowest score.
        if (check0 && check1 && check2)
        {
            print(highScore0);
            print(highScore1);
            print(highScore2);
            print(grabscore.score);
            if (scoreLength >= 10)
            {
                // assigns worst score to highest float value
                var worstScore = Mathf.Max(highScore0, highScore1, highScore2, grabscore.score);
                
                //if worst score is not the new score
                if (worstScore != grabscore.score)
                {
                    //finds which of existing scores is equal to worstscore
                    var worstScoreCheck0 = highScore0.Equals(worstScore);
                    var worstScoreCheck1 = highScore1.Equals(worstScore);
                    var worstScoreCheck2 = highScore2.Equals(worstScore);

                    //if worstscore is first score replace the firstscore following the score file format.
                    if (worstScoreCheck0)
                    {
                        File.WriteAllText(path, scores[0] + " " + baseScore + "\n" + name + " " + grabscore.score + "\n" + scores[4] + " " + highScore1 + "\n" + scores[6] + " " + highScore2 + "\n");
                    }
                    //if worstscore is second score replace the secondscore following the score file format.
                    if (worstScoreCheck1)
                    {
                        File.WriteAllText(path, scores[0] + " " + baseScore + "\n" + scores[2] + " " + highScore0 + "\n" + name + " " + grabscore.score + "\n" + scores[6] + " " + highScore2 + "\n");
                    }
                    //if worstscore is third score replace the thirdscore following the score file format.
                    if (worstScoreCheck2)
                    {
                        File.WriteAllText(path, scores[0] + " " + baseScore + "\n" + scores[2] + " " + highScore0 + "\n" + scores[4] + " " + highScore1 + "\n" + name + " " + grabscore.score + "\n");
                    }
                }
            }

        }
        //on click of save score button return to the startmenu.
        SceneManager.LoadScene("StartMenu");
        

    }

}

