﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class play : MonoBehaviour
{
    // pubblic void to be referenced on click of the start game button in the main menu and the retart button on the game complete screen and gameover screen.
    public void StartGame()
    {
        SceneManager.LoadScene("maze");
    }
}
