﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    //public void to be referenced on restart button from gameover and gamecomplete menus. gameobject is the gamecomplete screen.
    public GameObject gamecomplete;
    public void RestartGame()
    {
        gamecomplete.SetActive(false);
        SceneManager.LoadScene("maze");
    }
}
