﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    //need danger bool from detectionzone and player safe bool from player
    //serializefield allows the animator to be visible in inspector but still private variable.

    public Laser playerSituation;
    public Playermovment safe;
    [SerializeField] private Animator control;

    private void Start()
    {
       // print(control.GetLayerIndex("Base Layer"));
    }
    public void Update()

    {
        PlayerEnters();
        PlayerRetreats();
        PlayerEnters_Alcove();
    }

    //if player enters detectionzone set animation parameters to true or false
    void PlayerEnters()

    {
        var stateInfo = control.GetCurrentAnimatorStateInfo(0);

        if (playerSituation.danger == true && stateInfo.IsName("RetreatEnd"))
        {
            control.SetBool("Attack", true);
            control.SetBool("RetreatEnd", false);
        }

        if (playerSituation.danger == true && stateInfo.IsName("MovingWall_Retreat") && stateInfo.normalizedTime >= 1)
        {
            control.SetBool("Retreat", false);
            control.SetBool("RetreatEnd", true);
        }

    }

    //if player leaves detectionzone set animation parameters to true or false
    void PlayerRetreats()
    {
        var stateInfo = control.GetCurrentAnimatorStateInfo(0);      
        var stateAttackEnd = stateInfo.IsName("AttackEnd");
        var stateRetreat = stateInfo.IsName("MovingWall_Retreat");
        

        if (playerSituation.danger == false && safe.playerSafe == false && control.GetBool("Attack") == true && stateInfo.normalizedTime >= 1)
        {
            control.SetBool("Attack", false);
            control.SetBool("AttackEnd", true);
        }

        if (playerSituation.danger == false && safe.playerSafe == false && stateAttackEnd && stateInfo.normalizedTime >= 0.5)
        {
            control.SetBool("AttackEnd", false);
            control.SetBool("Retreat", true);
        }

        if (playerSituation.danger == false && stateRetreat && stateInfo.normalizedTime >= 1)
        {
            control.SetBool("Retreat", false);
            control.SetBool("RetreatEnd", true);
        }
    }

    //if player enters alcove set animation parameters to either true or false.
    void PlayerEnters_Alcove()
    {
        var stateInfo = control.GetCurrentAnimatorStateInfo(0);

        if (safe.playerSafe && stateInfo.IsName("MovingWall_Attack") && stateInfo.normalizedTime >=1)
        {
            control.SetBool("Attack", false);
            control.SetBool("AttackEnd", true);
        }

        if (safe.playerSafe && stateInfo.IsName("MovingWall_Retreat") && stateInfo.normalizedTime >= 1)
        {
            control.SetBool("Retreat", false);
            control.SetBool("RetreatEnd", true);
        }

    }

}
