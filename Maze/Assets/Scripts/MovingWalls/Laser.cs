﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    //creates danger bool to be used by other gameobjects as a trigger for events. 
    //using serializefield allows us to keep the animator control private, whilst still visible in the inspector.
    public bool danger;
    [SerializeField] private Animator control;
    
    //on entering the detection zone we are cheking what current animation state is and setting the danger bool accordingly
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && control.GetCurrentAnimatorStateInfo(0).IsName("RetreatEnd"))
        {
            print("Player in Danger!");
            danger = true;
        }

        else if (other.tag.Equals("Player") && control.GetCurrentAnimatorStateInfo(0).IsName("MovingWall_Retreat"))
        {
            print("Player in Danger!");
            danger = true;
        }

        
    }

    //if player leaves detection zone  danger bool = false.
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            print("Player Safe");
            danger = false;
            
        }
    }

}
