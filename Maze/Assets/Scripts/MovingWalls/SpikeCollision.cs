﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeCollision : MonoBehaviour
{
    // need health float from player
    //also need health icon gameobjects
    public Playermovment health;
    
    public GameObject health3;
    public GameObject health2;
    public GameObject health1;

    //ontrigger enter remove the first life if all lives present
    //if 2 lives present remove second life etc
    private void OnTriggerEnter(Collider other)
    {   
        if (other.tag.Equals("Player"))
        {
            health.playerHealth = health.playerHealth - 1;
            
            if (health.playerHealth == 2)
            {
                Health3();
            }
            if (health.playerHealth == 1)
            {
                Health2();
            }
            if (health.playerHealth == 0)
            {
                health.playerHealth = 0;
                Health1();
                
            }

        }
    }

    void Health3()
    {
        if (health3.activeInHierarchy)
        {
            health3.SetActive(false);
        }
        
    }
    void Health2()
    {
        if (health2.activeInHierarchy)
        {
            health2.SetActive(false);
        }
            
    }
    void Health1()
    {
        if (health1.activeInHierarchy)
        {
            health1.SetActive(false);
            
        }
    }


}
