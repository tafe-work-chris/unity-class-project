﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alcove : MonoBehaviour
{
    //need to grab player collider and playersafe bool from player as well as the danger bool from the detectionzones.
    public Collider player;
    public Playermovment safe;
    public Laser playerSituation;

    //if player enters an alcove they are now 'safe' and their detection is set to false.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            print("Player Safe");
            safe.playerSafe = true;
            playerSituation.danger = false;

        }
    }

    //if player leaves an alcove they are not 'safe'. this is because they will be entering a detection zone (detectionzone danger will be set true by ontrigger function of detectionzone)
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            print("Player in Danger");
            safe.playerSafe = false;
            
        }

    }
}
